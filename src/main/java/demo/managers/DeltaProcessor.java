package demo.managers;

import java.util.ArrayList;
import java.util.LinkedHashMap;
import java.util.List;

import javax.transaction.Transactional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import demo.models.DeltaItem;
import demo.models.DeltaShoppingList;
import demo.models.Item;
import demo.models.ResponseUpdateDelta;
import demo.models.ShoppingList;
import demo.repositories.ItemRepository;
import demo.repositories.ShoppingListRepository;

@Service
public class DeltaProcessor {
	
	@Autowired
	ItemRepository itemsRepo;
	
	@Autowired
	ShoppingListRepository shoppingListsRepo;
	
	public ResponseUpdateDelta processDeltaList(DeltaShoppingList deltaList){
		DeltaShoppingList resultDelta = new DeltaShoppingList();
		ShoppingList resultShoppingList = new ShoppingList();
		
		switch(deltaList.getStatus()){
		case ADD: 
			resultShoppingList = addNewShoppingList(deltaList); 
			return new ResponseUpdateDelta(resultDelta, resultShoppingList);
		case REMOVE: 
			resultDelta = removeShoppingList(deltaList);
			resultShoppingList = shoppingListsRepo.findOne(deltaList.getId());
			return new ResponseUpdateDelta(resultDelta, resultShoppingList);
		case EDIT: 
			resultDelta = editShoppingList(deltaList, true);
			resultShoppingList = shoppingListsRepo.findOne(deltaList.getId());
			return new ResponseUpdateDelta(resultDelta, resultShoppingList);
		case NONE: 
			resultDelta = editShoppingList(deltaList, false);
			resultShoppingList = shoppingListsRepo.findOne(deltaList.getId());
			return new ResponseUpdateDelta(resultDelta, resultShoppingList);
		}
		return null;
	}
	
	@Transactional
	private ShoppingList addNewShoppingList(DeltaShoppingList deltaList){
		Long timestamp = System.currentTimeMillis();
		ShoppingList shoppingList = new ShoppingList();
		shoppingList.setListName(deltaList.getListName());
		shoppingList.setLastUpdate(timestamp);
		List<Item> listItem = new ArrayList();
		for (DeltaItem deltaItem : deltaList.getDeltaItems()){
			Item item = itemsRepo.saveAndFlush(deltaItem.getNewItemFromDelta());
			listItem.add(item);
		}
		shoppingList.setItems(listItem);
		return shoppingListsRepo.saveAndFlush(shoppingList);
	}
	
	@Transactional
	private DeltaShoppingList removeShoppingList(DeltaShoppingList deltaList){
		Long timestamp = System.currentTimeMillis();
		ShoppingList shoppingList = shoppingListsRepo.findOne(deltaList.getId());
		if (shoppingList == null){
			//Somebody already deleted list
			return null;
		}
		if (shoppingList.getLastUpdate() > deltaList.getLastUpdate()){
			//Somebody updated list after last sync
			deltaList.setLastUpdate(timestamp);
			return deltaList;
		}
		else {
			//Nobody updated list after last sync
			shoppingListsRepo.delete(deltaList.getId());
			shoppingListsRepo.flush();
			return null;
		}
	}
	
	@Transactional
	private DeltaShoppingList editShoppingList(DeltaShoppingList deltaList, Boolean updateList){
		Long timestamp = System.currentTimeMillis();
		ShoppingList shoppingList = shoppingListsRepo.findOne(deltaList.getId());
		DeltaShoppingList unpushedDelta = new DeltaShoppingList();
		if (shoppingList == null){
			//Somebody already deleted list.
			return null;
		}
		if (updateList){
			if (shoppingList.getLastUpdate() > deltaList.getLastUpdate()){
				//Somebody updated list after last sync
				unpushedDelta.setId(shoppingList.getId());
				unpushedDelta.setLastUpdate(timestamp);
				unpushedDelta.setListName(deltaList.getListName());
				unpushedDelta.setStatus(deltaList.getStatus());
			} else {
				//update shoppingList
				shoppingList.setLastUpdate(timestamp);
				shoppingList.setListName(deltaList.getListName());
			}
		}
		List<DeltaItem> unpushedItems = updateItems(shoppingList, deltaList.getDeltaItems());
		unpushedDelta.setDeltaItems(unpushedItems);
		shoppingListsRepo.saveAndFlush(shoppingList);
		return unpushedDelta;
	}
	
	@Transactional
	private List<DeltaItem> updateItems(ShoppingList current, List<DeltaItem> deltaItems){
		Long timestamp = System.currentTimeMillis();
		LinkedHashMap<Long, Item> shoppingListItems = new LinkedHashMap<>();
		List<DeltaItem> unpusedItems = new ArrayList<DeltaItem>();
		for (Item item : current.getItems()){
			shoppingListItems.put(item.getId(), item);
		}
		for (DeltaItem delta : deltaItems){
			try{
				switch (delta.getStatus()){
				case ADD: {
					Item newItem = delta.getNewItemFromDelta();
					newItem = itemsRepo.saveAndFlush(newItem);
					shoppingListItems.put(newItem.getId(), newItem);
					break;
				}
				case REMOVE: {
					Item item = itemsRepo.findOne(delta.getId());
					if (item == null || item.getLastUpdate() > delta.getLastUpdate()){
						delta.setLastUpdate(timestamp);
						unpusedItems.add(delta);
					} else {
						shoppingListItems.remove(delta.getId());
						itemsRepo.delete(delta.getId());
					}
					break;
				}
				case EDIT: {
					Item item = itemsRepo.findOne(delta.getId());
					if (item == null || item.getLastUpdate() > delta.getLastUpdate()){
						delta.setLastUpdate(timestamp);
						unpusedItems.add(delta);
					} else {
						item.updateItem(delta, timestamp);
						shoppingListItems.put(item.getId(), item);
						itemsRepo.save(item);
					}
					break;
				}
				}
			} catch (Exception ex) {
				ex.printStackTrace();
			}
		}
		current.setItems(new ArrayList(shoppingListItems.values()));
		itemsRepo.flush();
		
		return unpusedItems;
	}
}
