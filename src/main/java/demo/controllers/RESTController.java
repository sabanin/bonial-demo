package demo.controllers;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import demo.managers.DeltaProcessor;
import demo.models.DeltaShoppingList;
import demo.models.ResponseUpdateDelta;


@RestController
public class RESTController {
	
	@Autowired
	DeltaProcessor deltaProcessor;
	
	@RequestMapping(method = RequestMethod.POST, value = "/api/lists")
	public @ResponseBody ResponseEntity<ResponseUpdateDelta> updateData(@RequestBody DeltaShoppingList deltaList) {
		try{
			return new ResponseEntity<ResponseUpdateDelta>(deltaProcessor.processDeltaList(deltaList), HttpStatus.OK);
		} catch (Exception ex){
			ex.printStackTrace();
			return new ResponseEntity<ResponseUpdateDelta>(HttpStatus.BAD_REQUEST);
		}
	}
	
}


