package demo.models;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.xml.bind.annotation.XmlRootElement;

import lombok.Data;

@Entity
@Data
public class Item {
	@Id 
	@GeneratedValue(strategy = GenerationType.AUTO)
	Long id;
	String itemName;
	Boolean isMarkedAsChecked;
	String quantityDescription;
	Long lastUpdate;
	
	public void updateItem(DeltaItem deltaItem, Long timestamp){
		itemName = deltaItem.getItemName();
		isMarkedAsChecked = deltaItem.getIsMarkedAsChecked();
		quantityDescription = deltaItem.getQuantityDescription();
		lastUpdate = timestamp;
	}
}
