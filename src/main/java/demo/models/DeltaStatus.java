package demo.models;

public enum DeltaStatus {
	ADD,
	REMOVE,
	EDIT,
	NONE
}
