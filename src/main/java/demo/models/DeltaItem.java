package demo.models;

import lombok.Data;

@Data
public class DeltaItem extends Item{
	DeltaStatus status;
	
	public Item getNewItemFromDelta(){
		Item item = new Item();
		item.setIsMarkedAsChecked(isMarkedAsChecked);
		item.setItemName(itemName);
		item.setLastUpdate(0L);
		item.setQuantityDescription(quantityDescription);
		return item;
	}
	
}
