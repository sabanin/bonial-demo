package demo.models;

import lombok.Data;

@Data
public class ResponseUpdateDelta {
	DeltaShoppingList delta;
	ShoppingList shoppingList;
	
	public ResponseUpdateDelta(DeltaShoppingList deltaShoppingList, ShoppingList shoppingList){
		this.delta = deltaShoppingList;
		this.shoppingList = shoppingList;
	}
}
