package demo.models;

import java.util.List;

import javax.persistence.CascadeType;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.OneToMany;
import javax.persistence.OneToOne;
import javax.xml.bind.annotation.XmlRootElement;

import lombok.Data;

@Entity
@Data
public class ShoppingList {
	@Id 
	@GeneratedValue(strategy = GenerationType.AUTO)
	Long id;
	String listName;
	@OneToMany(fetch = FetchType.EAGER)
	List<Item> items;
	Long lastUpdate;
}
