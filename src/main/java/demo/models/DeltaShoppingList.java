package demo.models;

import java.util.ArrayList;
import java.util.List;

import lombok.Data;

@Data
public class DeltaShoppingList extends ShoppingList{
	DeltaStatus status;
	List<DeltaItem> deltaItems = new ArrayList();
}
