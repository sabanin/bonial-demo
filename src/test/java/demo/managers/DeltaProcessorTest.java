package demo.managers;

import static org.junit.Assert.*;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.SpringApplicationConfiguration;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;
import org.springframework.test.context.web.WebAppConfiguration;

import demo.DemoApplication;
import demo.SimpleDbConfig;
import demo.models.DeltaShoppingList;
import demo.models.DeltaStatus;
import demo.models.ResponseUpdateDelta;

import static org.hamcrest.MatcherAssert.assertThat;
import static org.hamcrest.Matchers.equalTo;
import static org.hamcrest.Matchers.is;
import static org.hamcrest.core.IsNull.notNullValue;

@RunWith(SpringJUnit4ClassRunner.class)
@SpringApplicationConfiguration(classes = { DemoApplication.class,
		SimpleDbConfig.class })
@WebAppConfiguration
public class DeltaProcessorTest {

	@Autowired
	DeltaProcessor deltaProcessor;
	
	@Test
	public void testCreateListSucess() {
		DeltaShoppingList deltaList = new DeltaShoppingList();
		deltaList.setStatus(DeltaStatus.ADD);
		deltaList.setListName("Test");
		
		ResponseUpdateDelta rud = deltaProcessor.processDeltaList(deltaList);
		
		assertNotNull(rud.getShoppingList().getId());
		assertNotNull(rud.getShoppingList().getLastUpdate());
		assertNull(rud.getDelta().getId());
		assertNull(rud.getDelta().getLastUpdate());
		assertEquals(rud.getShoppingList().getListName(), "Test");
	}
	
	@Test
	public void testUpdateListSucess() {
		DeltaShoppingList deltaList = new DeltaShoppingList();
		deltaList.setStatus(DeltaStatus.ADD);
		deltaList.setListName("Test");
		ResponseUpdateDelta rud = deltaProcessor.processDeltaList(deltaList);		
		deltaList.setLastUpdate(rud.getShoppingList().getLastUpdate());
		deltaList.setId(rud.getShoppingList().getId());
		deltaList.setListName("TestEdit");
		deltaList.setStatus(DeltaStatus.EDIT);
		
		rud = deltaProcessor.processDeltaList(deltaList);

		assertNotNull(rud.getShoppingList().getId());
		assertNotNull(rud.getShoppingList().getLastUpdate());
		assertNull(rud.getDelta().getId());
		assertNull(rud.getDelta().getLastUpdate());
		assertEquals(rud.getShoppingList().getListName(), "TestEdit");
		
	}
	
	@Test
	public void testUpdateListFail() {
		DeltaShoppingList deltaList = new DeltaShoppingList();
		deltaList.setStatus(DeltaStatus.ADD);
		deltaList.setListName("Test");
		ResponseUpdateDelta rud = deltaProcessor.processDeltaList(deltaList);		
		deltaList.setLastUpdate(0L);
		deltaList.setId(rud.getShoppingList().getId());
		deltaList.setListName("TestEdit");
		deltaList.setStatus(DeltaStatus.EDIT);
		
		rud = deltaProcessor.processDeltaList(deltaList);

		assertNotNull(rud.getShoppingList().getId());
		assertNotNull(rud.getShoppingList().getLastUpdate());
		assertNotNull(rud.getDelta().getId());
		assertNotNull(rud.getDelta().getLastUpdate());
		assertEquals(rud.getDelta().getStatus(), DeltaStatus.EDIT);
		assertNotEquals(rud.getShoppingList().getListName(), "TestEdit");
		
	}
	
	//todo: More tests to implement: successfull and not successfull add/remove/edit of items
	
}
